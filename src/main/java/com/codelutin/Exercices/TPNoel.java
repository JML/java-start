package com.codelutin.Exercices;

import java.util.Scanner;

public class TPNoel {
    public static void main(String[] args) {
        String week[] = {"lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"};
        int year, gap;
        String noel;
        year=0;
        while(year < 2000 || year > 2099) {
            System.out.println("Veuillez entrer une année entre 2000 et 2099 pour laquelle vous voulez savoir le jour de Noël:");
            Scanner keyboardInput = new Scanner(System.in);
            year = keyboardInput.nextInt();
        }
        gap = ((year - 2000) + (year - 2000) / 4) % 7;
        noel = week[gap];
        System.out.println("Noël en " + year + " est un " + noel);
    }
}
