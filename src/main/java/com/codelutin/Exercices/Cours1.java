package com.codelutin.Exercices;

import java.util.Scanner;

public class Cours1 {

    public static void main(String[] args) {

        int nombre1, nombre2;

        Scanner keyboardInput = new Scanner(System.in);
        System.out.println("Donne moi un premier nombre:");
        nombre1 = keyboardInput.nextInt();
        System.out.println("Donne moi un second nombre:");
        nombre2 = keyboardInput.nextInt();

        if (nombre1 > nombre2) {
            System.out.println(nombre1 + " est plus grand que " + nombre2);
        } else if (nombre1 < nombre2) {
            System.out.println(nombre1 + " est plus petit que " + nombre2);
        } else {
            System.out.println("Les deux nombres sont égaux.");
        }
    }
}
